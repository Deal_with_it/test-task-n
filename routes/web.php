<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('order.create');
});

Route::get('order/create', 'OrderController@create')->name('order.create');
Route::post('order', 'OrderController@store')->name('order.store');

Route::get('statistics', 'SalesStatisticsController@index')->name('statistics.index');
Route::get('statistics/getData', 'SalesStatisticsController@getData')->name('statistics.getData');

Route::resources([
    'categories' => 'CategoryController',
    'products' => 'ProductController'
]);