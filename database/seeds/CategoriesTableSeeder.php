<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Category::create(['name' => 'Weapons']);
        App\Models\Category::create(['name' => 'Cars']);
    }
}
