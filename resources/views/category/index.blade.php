@extends('base')

@section('content')

@if (Session::has('message'))
  <div class="alert alert-success" role="alert">
    {!! session('message') !!}
  </div>
@endif

<div class="d-flex justify-content-between">
    <div><h2>Categories</h2></div>
    <div>
        <a href=" {{ route('categories.create') }}">
            <button type="button" class="btn btn-primary">Add category</button>
        </a>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
          @foreach ($categories as $category)
                <tr>
                  <td>{{ $category->id }}</td>
                  <td>{{ $category->name }}</td>
                  <td>
                    <a href="{{ route('categories.edit', ['id' => $category->id])}}">
                      <button class="btn btn-info btn-sm">Edit</button>
                    </a>
                  </td>
                  <td>
                    <button data-id="{{ $category->id }}" data-href="{{ route('categories.destroy', ['id' => '_ID_']) }}" class="btn btn-danger btn-sm">
                        Delete
                    </button>
                  </td>
                <tr>
          @endforeach
      </tbody>
    </table>
</div>

<script>
    function deleteCategory(event) { 
        let data = event.target.dataset;
        let isConfirm = confirm("Are you sure you want to delete this category?");

        if (isConfirm) {
            $.ajax({
                url: data.href.replace('_ID_', data.id),
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function() {
                    deleteRow(event);
                },
                error: function() {
                    alert( "An error occured" );
                },
            })
        }   
    }

    $('.btn-danger').click(deleteCategory); 
</script>
@endsection
