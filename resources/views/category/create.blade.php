@extends('base')

@section('content')
<h2>Create category</h2>
<form action="{{ route('categories.store') }}" method="POST">
    @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input class="form-control" id="name" name="name" required>
    </div>
    <button type="submit" class="btn btn-primary">Create</button>
  </form>
@endsection