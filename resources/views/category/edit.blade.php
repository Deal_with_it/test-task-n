@extends('base')

@section('content')
<h2>Edit category</h2>
<form action="{{ route('categories.update', ['id' => $category->id]) }}" method="POST">
    @method('PUT')
    @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input class="form-control" id="name" name="name" value="{{ $category->name }}" required>
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection