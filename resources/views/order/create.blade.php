@extends('base')

@section('content')

@if (Session::has('message'))
  <div class="alert alert-success" role="alert">
    {!! session('message') !!}
  </div>
@endif

<h2>Order products</h2>
<form action="{{ route('order.store') }}" method="POST">
    @csrf
    <div class="form-group">
        <label for="category_id">Category</label>
        <select class="custom-select" name="category_id" required>
            
            @foreach ($categories as $category)
                <option value="{{$category->id}}">{{ $category->name }}</option>
            @endforeach            
        
        </select>
    </div>
    <div class="form-group">
        <label for="product_id">Product</label>
        <select class="custom-select" name="product_id" required>
            
            @foreach ($categories->first()->products as $product)
                <option value="{{$product->id}}">{{ $product->name }}</option> 
            @endforeach
        
        </select>
    </div>
    <div class="form-group">
        <label for="quantity">Quantity</label>
        <input type="number" class="form-control" id="quantity" name="quantity" required>
    </div>
    <button type="submit" class="btn btn-primary">Create</button>
</form>

<script>
    let productData = {!! json_encode($categories) !!};
    let productSelect = $("select[name='product_id']");

    // refresh list of products after selecting category
    $("select[name='category_id']").change(function(e) {
        let products = productData[e.target.selectedIndex].products;
        let productOptions = "";

        for(let i=0; i < products.length; i++) {
            productOptions+= `<option value="${products[i].id}">${products[i].name}</option>`;
        }

        productSelect.html(productOptions);

    });
</script>
@endsection