@extends('base')

@section('content')
<h2>Create product</h2>
<form action="{{ route('products.store') }}" method="POST">
    @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input class="form-control" id="name" name="name" required>
    </div>
    <div class="form-group">
        <label for="name">Price</label>
        <input class="form-control" id="price" name="price" required>
    </div>
    <div class="form-group">
        <select class="custom-select" required name="category_id">
            <option selected value="">Choose category</option>
            @foreach ($categories as $category)
                <option value="{{$category->id}}">{{ $category->name }}</option>
            @endforeach
            
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Create</button>
  </form>
@endsection