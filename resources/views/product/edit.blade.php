@extends('base')

@section('content')
<h2>Create product</h2>
<form action="{{ route('products.update', ['id' => $product->id]) }}" method="POST">
    @method('PUT')
    @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input class="form-control" id="name" name="name" value="{{ $product->name }}" required>
    </div>
    <div class="form-group">
        <label for="name">Price</label>
    <input class="form-control" id="price" name="price" value="{{ $product->price }}" required>
    </div>
    <div class="form-group">
        <select class="custom-select" required name="category_id">
            @foreach ($categories as $category)
                <option value="{{$category->id}}"
                    @if ($category->id == $product->category_id)
                        selected
                    @endif>{{ $category->name }}</option>
            @endforeach
            
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection