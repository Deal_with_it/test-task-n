@extends('base')

@section('content')

@if (Session::has('message'))
  <div class="alert alert-success" role="alert">
    {!! session('message') !!}
  </div>
@endif

<div class="d-flex justify-content-between">
    <div><h2>Products</h2></div>
    <div>
        <a href=" {{ route('products.create') }}">
            <button type="button" class="btn btn-primary">Add product</button>
        </a>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Price</th>
          <th>Category</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
          @foreach ($products as $product)
                <tr>
                  <td>{{ $product->id }}</td>
                  <td>{{ $product->name }}</td>
                  <td>{{ $product->price . '$' }}</td>
                  <td>{{ $product->category->name }}</td>
                  <td>
                    <a href="{{ route('products.edit', ['id' => $product->id])}}">
                      <button class="btn btn-info btn-sm">Edit</button>
                    </a>
                  </td>
                  <td>
                    <button data-id="{{ $product->id }}" data-href="{{ route('products.destroy', ['id' => '_ID_']) }}" class="btn btn-danger btn-sm">
                        Delete
                    </button>
                  </td>
                <tr>
          @endforeach
      </tbody>
    </table>
</div>

<script>
    function deleteProduct(event) { 
        let data = event.target.dataset;
        let isConfirm = confirm("Are you sure you want to delete this product?");

        if (isConfirm) {
            $.ajax({
                url: data.href.replace('_ID_', data.id),
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function() {
                    deleteRow(event);
                },
                error: function() {
                    alert( "An error occured" );
                },
            })
        }   
    }

    $('.btn-danger').click(deleteProduct); 
</script>
@endsection
