@extends('base')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datepicker3.standalone.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>    
@endpush

@section('content')
<h2>Sales statistics</h2>
<form action="{{ route('statistics.getData') }}">
    <div class="form-group">
        <label for="from_date">From</label>
        <input class="d-picker" name="from_date" required autocomplete="off">
        <label for="to_date">To</label>
        <input class="d-picker" name="to_date" required autocomplete="off">
    </div>
    <div class="form-group">
        <select class="custom-select" name="category_id">
            <option selected value="">Choose category</option>
            @foreach ($categories as $category)
                <option value="{{$category->id}}">{{ $category->name }}</option>
            @endforeach            
        
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<div class="row">
<canvas  id="myChart" width="500" height="200"></canvas></div>
<script>
    
</script>
<script>
    // Initialize datepickers
    $('.d-picker').datepicker({
        uiLibrary: 'bootstrap4'
    })

    let form = $('form');
    form.submit(function(e) {
        e.preventDefault();

        let action = this.action + '?' + $(this).serialize();
        $.ajax({
            url: action,
            dataType: 'json',
            success: function(data) {
                dd= data;
                if (data) {
                    buildChart(data);
                }
            },
            error: function() {
                alert( "An error occured" );
            },
        })
    });

    // 
    var chart;

    /**
     * Generate random color
     * @return string color
     */
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    /**
     *
     */
    function buildChart(rawData) {
        let labels = [];
        let data = [];
        let backgroundColor = [];
        
        for (const property in rawData) {
            let label = `${property}. ${rawData[property].quantity} units sold`;
            labels.push(label);
            data.push(rawData[property].totalPrice);
            backgroundColor.push(getRandomColor());
        }

        if (chart) {
            chart.destroy();
        }

        drawChart(labels, data, backgroundColor);
    }

    /**
     * Draw sales statistics chart
     */
    function drawChart(labels, data, backgroundColor) {
        var canvas = document.getElementById('myChart');
        var ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: '# for Total price',
                    data: data,
                    backgroundColor: backgroundColor,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }
</script>
@endsection