<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Order;
use App\Models\Product;

use Carbon\Carbon;

class SalesStatistics
{
    protected $requestData;

    /**
     *
     * @param Array $requestData
     */
    public function __construct(Array $requestData)
    {
        $this->requestData = $requestData;
    }

    /**
     * Get sales statistics data
     *
     * @return Collection
     */
    public function getData()
    {
        $orders = $this->getOrdersBetweenDates($this->requestData['from_date'], $this->requestData['to_date']);

        if ($this->requestData['category_id']) {
            $orderGroups= $orders->where('category_id', $this->requestData['category_id'])->get()->groupBy('product_id');
            $keys = Product::all();
        } else {
            $orderGroups = $orders->get()->groupBy('category_id');
            $keys = Category::all();
        }

        return $this->calculate($orderGroups, $keys);
    }

    /**
     * Calculate how much was ordered
     *
     * @param Collection $orderGroups
     * @param Collection $keys
     * @return Illuminate\Support\Collection
     */
    public function calculate($orderGroups, $keys)
    {
        $data = collect([]);
        foreach ($orderGroups as $key => $group) {
            $totalQuantity = $group->sum('quantity');
            $totalPrice = $group->sum('total_price');
            $groupName = $keys->find($key)->name;
            
            $data->put($groupName, ['quantity' => $totalQuantity, 'totalPrice' => $totalPrice]);
        }

        return $data;
    }

    /**
     *
     * @param string $fromDate
     * @param string $toDate
     * @return Illuminate\Database\Eloquent\Builder 
     */
    public function getOrdersBetweenDates($fromDate, $toDate)
    {
        $fromDate = $fromDate ? Carbon::parse($fromDate) : Carbon::today();
        $toDate = $toDate ? Carbon::parse($toDate)->addDay() : Carbon::today()->addDay();
        $orders = Order::whereBetween('created_at', [$fromDate, $toDate]);

        return $orders;
    }
}