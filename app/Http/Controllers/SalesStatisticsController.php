<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;
use App\Services\SalesStatistics;

use Carbon\Carbon;

class SalesStatisticsController extends Controller
{
    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('sales_statistics.index', ['categories' => Category::all()]);
    }

    /**
     * Get sales statistics data
     *
     * @return Json
     */
    public function getData(Request $request)
    {
        $request->validate([
            'from_date' => 'date',
            'to_date' => 'date',
        ]);

        $data = (new SalesStatistics($request->all()))->getData();
        return json_encode($data);
    }
}
