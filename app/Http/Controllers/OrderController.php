<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;
use App\Models\Order;
use App\Models\Product;

class OrderController extends Controller
{
    /**
     * Show the form for creating a new order.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = Category::with('products')->get();
        return view('order.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created order in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required|integer|min:1',
            'product_id' => 'required|integer|min:1',
            'quantity' => 'required|integer|min:1'
        ]);

        $order = new Order($request->only('category_id', 'product_id', 'quantity'));
        $order->total_price = Product::find($order->product_id)->price * $order->quantity;
        $order->save();

        $request->session()->flash('message', 'Order has been created.');
        return redirect()->action('OrderController@create');
    }
}
