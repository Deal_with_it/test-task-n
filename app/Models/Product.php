<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'price', 'category_id'];

    /**
     * Get the category associated with the product
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Get the orders associated with the product
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}
