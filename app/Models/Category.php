<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    /**
     * Get the products associated with the category
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    /**
     * Get the orders associated with the category
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}
