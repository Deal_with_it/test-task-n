<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['category_id', 'product_id', 'quantity'];

    /**
     * Get the category associated with the order
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Get the product associated with the order
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
